# **What did we learn today? What activites did you do? What scenes have impressed you**
We focused on three design patterns today and some considerations in refactoring code. This afternoon, we also practiced code refactoring ourselves, and I was most impressed by this refactoring, it allowed me to avoid smelly code and make my code writing more standardized and easy to understand.
# **Pleas use one word to express your feelings about today's class.**
useful
# **What do you think about this? What was the most meaningful aspect of this activity?**
I think one of the most useful today is the learning of refactoring considerations, which makes our code easier to read.
# **Where do you most want to apply what you have learned today? What changes will you make?**
I will apply what I learn today to my future development and standardize my code.


https://gitlab.com/Hilton7/mars-rover-starter