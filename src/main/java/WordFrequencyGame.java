import java.util.*;

public class WordFrequencyGame {
    private final int INIT_COUNT = 1;
    private final String SEPARATION_CONFORMS = "\\s+";

    public String getResult(String inputStr) {
        try {
            //split the input string with 1 to n pieces of spaces
            List<Input> inputList = getInputList(inputStr);

            //get the map for the next step of sizing the same word
            Map<String, List<Input>> wordCategoriesMap = getwordCategoriesMap(inputList);

            //get the resultList by wordCategoriesMap
            List<Input> resultList = getResultList(wordCategoriesMap);

            //sort the result list DESC
            sortListDESC(resultList);

            //get the result string
            StringJoiner resultStrJoiner = new StringJoiner("\n");
            return getResultString(resultList, resultStrJoiner);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private String getResultString(List<Input> inputList, StringJoiner resultStr) {
        inputList.stream().forEach(word -> {
            String resultItem = word.getValue() + " " + word.getWordCount();
            resultStr.add(resultItem);
        });
        return resultStr.toString();
    }

    private void sortListDESC(List<Input> resultList) {
        resultList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private List<Input> getResultList(Map<String, List<Input>> wordCategoriesMap) {
        List<Input> resultList = new ArrayList<>();
        for (Map.Entry<String, List<Input>> entry : wordCategoriesMap.entrySet()) {
            Input input = new Input(entry.getKey(), entry.getValue().size());
            resultList.add(input);
        }
        return resultList;
    }

    private List<Input> getInputList(String inputStr) {
        String[] arrWords = inputStr.split(SEPARATION_CONFORMS);
        List<Input> inputList = new ArrayList<>();
        for (String word : arrWords) {
            Input input = new Input(word, INIT_COUNT);
            inputList.add(input);
        }
        return inputList;
    }


    private Map<String, List<Input>> getwordCategoriesMap(List<Input> inputList) {
        Map<String, List<Input>> wordCategoriesMap = new HashMap<>();
        inputList.stream().forEach(input -> {
            if (!wordCategoriesMap.containsKey(input.getValue())) {
                ArrayList arr = new ArrayList<>();
                arr.add(input);
                wordCategoriesMap.put(input.getValue(), arr);
            } else {
                wordCategoriesMap.get(input.getValue()).add(input);
            }
        });
        return wordCategoriesMap;
    }


}
